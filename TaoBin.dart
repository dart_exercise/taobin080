import 'dart:io';

class TaoBin {
  var type, menu, typeName, menuName, sweet, straw, lid, strawYN, lidYN, balance;
  int cash = 0, price = 0;

  //Choose Type
  void Type() {
    String chooseType = ''' 
  Choose the type of drinks
  1. Coffee
  2. Tea
  3. Protein Shakes
  4. Fruity Drinks
  5. Soda
  6. Recommended Menu

  ''';
    stdout.write(chooseType);
  }

  //Choose Menu (Type)
  void Menu(var type) {
    //Coffee
    if (type == "1") {
      String menuCoffee = ''' 
  Type : Coffee
  1. Espresso
  2. Americano
  3. Mocha
  4. Cappuccino
  5. Iced Kokuto Cafe
  6. Iced Cannabis
  
  ''';
      stdout.write(menuCoffee);
    }
    //Tea
    else if (type == "2") {
      String menuTea = ''' 
  Type : Tea
  1. Iced Tea
  2. Iced Matcha Latte
  3. Iced Lychee Tea
  4. Hot Lime Tea
  5. Hot Kokuto Tea
  6. Hot Black Tea
  
  ''';
      stdout.write(menuTea);
    }
    //Protein Shakes
    else if (type == "3") {
      String menuPS = ''' 
  Type : Protein Shakes
  1. Matcha Protein Shake
  2. Milk Protein Shake
  3. Thai Tea Protein Shake
  4. Caramel Protein Shake
  5. Chocolate Protein Shake
  6. Plain Protein Shake
  
  ''';
      stdout.write(menuPS);
    }
    //Fruity Drinks
    else if (type == "4") {
      String menuFruity = ''' 
  Type : Fruity Drinks
  1. Hot Limeade
  2. Ice Limeade
  3. Ice Lychee
  4. Ice Plum
  5. Ice Mango
  6. Ice Sala
  
  ''';
      stdout.write(menuFruity);
    }
    //Soda
    else if (type == "5") {
      String menuSoda = ''' 
  Type : Soda
  1. Pepsi
  2. Ice Limeade Soda
  3. Ice Strawberry Soda
  4. Ice Plum Soda
  5. Ice Lychee Soda
  6. Ice Blueberry Soda
  
  ''';
      stdout.write(menuSoda);
    }
    //Recommended
    else if (type == "6") {
      String menuRecomm = ''' 
  Type : Recommended
  1. Iced Cannabis
  2. Iced Cafe Latte
  3. Iced Tea
  4. Hot Cocoa
  5. Hot Kokuto Milk
  6. Hot Black Tea
  
  ''';
      stdout.write(menuRecomm);
    }
  }

//Choose Price(Type, Menu)
  void Price(type, menu) {
    if (type == "1") {
      typeName = "Coffee";
      //Menu
      if (menu == "1") {
        menuName = "Espresso";
        price = 50;
      } else if (menu == "2") {
        menuName = "Americano";
        price = 50;
      } else if (menu == "3") {
        menuName = "Mocha";
        price = 50;
      } else if (menu == "4") {
        menuName = "Cappuccino";
        price = 50;
      } else if (menu == "5") {
        menuName = "Iced Kokuto Cafe";
        price = 50;
      } else if (menu == "6") {
        menuName = "Ice Cannabis";
        price = 60;
      }
    } else if (type == "2") {
      typeName = "Tea";
      //Menu
      if (menu == "1") {
        menuName = "Iced Tea";
        price = 40;
      } else if (menu == "2") {
        menuName = "Iced Matcha Latte";
        price = 40;
      } else if (menu == "3") {
        menuName = "Iced Lychee Tea";
        price = 40;
      } else if (menu == "4") {
        menuName = "Hot Lime Tea";
        price = 40;
      } else if (menu == "5") {
        menuName = "Hot Kokuto Tea";
        price = 40;
      } else if (menu == "6") {
        menuName = "Hot Black Tea";
        price = 40;
      }
    } else if (type == "3") {
      typeName = "Protein Shakes";
      //Menu
      if (menu == "1") {
        menuName = "Matcha Protein Shake";
        price = 55;
      } else if (menu == "2") {
        menuName = "Milk Protein Shake";
        price = 50;
      } else if (menu == "3") {
        menuName = "Thai Tea Protein Shake";
        price = 55;
      } else if (menu == "4") {
        menuName = "Caramel Protein Shake";
        price = 55;
      } else if (menu == "5") {
        menuName = "Chocolate Protein Shake";
        price = 55;
      } else if (menu == "6") {
        menuName = "Plain Protein Shake";
        price = 55;
      }
    } else if (type == "4") {
      typeName = "Fruity Drinks";
      //Menu
      if (menu == "1") {
        menuName = "Hot Limeade";
        price = 40;
      } else if (menu == "2") {
        menuName = "Ice Limeade";
        price = 40;
      } else if (menu == "3") {
        menuName = "Ice Lychee";
        price = 45;
      } else if (menu == "4") {
        menuName = "Ice Plum";
        price = 45;
      } else if (menu == "5") {
        menuName = "Ice Mango";
        price = 45;
      } else if (menu == "6") {
        menuName = "Ice Sala";
        price = 45;
      }
    } else if (type == "5") {
      typeName = "Soda";
      //Menu
      if (menu == "1") {
        menuName = "Pepsi";
        price = 30;
      } else if (menu == "2") {
        menuName = "Ice Limeade Soda";
        price = 50;
      } else if (menu == "3") {
        menuName = "Ice Strawberry Soda";
        price = 50;
      } else if (menu == "4") {
        menuName = "Ice Plum Soda";
        price = 50;
      } else if (menu == "5") {
        menuName = "Ice Lychee Soda";
        price = 50;
      } else if (menu == "6") {
        menuName = "Ice Blueberry Soda";
        price = 50;
      }
    } else if (type == "6") {
      typeName = "Recommended";
      //Menu
      if (menu == "1") {
        menuName = "Iced Cannabis";
        price = 60;
      } else if (menu == "2") {
        menuName = "Iced Cafe Latte";
        price = 45;
      } else if (menu == "3") {
        menuName = "Iced Tea";
        price = 40;
      } else if (menu == "4") {
        menuName = "Hot Cocoa";
        price = 45;
      } else if (menu == "5") {
        menuName = "Hot Kokuto Milk";
        price = 45;
      } else if (menu == "6") {
        menuName = "Hot Black Tea";
        price = 40;
      }
    }
  }

  //Sweetness Level
  void sweetLV() {
    String Level = '''
  Sweetness Level
  1. No Sugar
  2. Less Sweet
  3. Just right
  4. Sweet
  5. Very Sweet
''';

    stdout.write(Level);
  }

  //Choose Sweetness Level
  void sweetChoose(Level) {
    if (Level == "1") {
      sweet = "No Sugar";
    } else if (Level == "2") {
      sweet = "Less Sweet";
    } else if (Level == "3") {
      sweet = "Just right";
    } else if (Level == "4") {
      sweet = "Sweet";
    } else if (Level == "5") {
      sweet = "Very Sweet";
    }
  }

  //Straw & Lid 1=Y 2=N
  void StrawnLid(Straw, Lid) {
    //11, 12
    if (Straw == "1") {
      straw = "Yes";
      strawYN = "Y";
      if (Lid == "1") {
        lid = "Yes";
        lidYN = "Y";
      } else if (Lid == "2") {
        lid = "No";
        lidYN = "N";
      }
    }
    //21, 22
    else if (Straw == "2") {
      straw = "No";
      strawYN = "N";
      if (Lid == "1") {
        lid = "Yes";
        lidYN = "Y";
      } else if (Lid == "2") {
        lid = "No";
        lidYN = "N";
      }
    }
  }

  void charge( cash, price){
    balance = cash - price;
  }

  @override
  String toString() {
    return " ========== Receipt ========== \n" + "\n Type: "  + typeName + "\n Menu: " + menuName + 
    "\n Price: $price฿" + "\n SweetLevel: " + sweet + "\n Straw&Lid: " + strawYN + "/" + lidYN +
    "\n Cash: $cash฿" + "\n \n Balance: $balance฿" + "\n ======== Thanks you! ======== ";
  }
}
